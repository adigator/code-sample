//
//  SingleNewsSerializer.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import SwiftyJSON

fileprivate struct Keys {
    static let id = "id"
    static let title = "title"
    static let body = "body"
}

class SinglePostSerializer {
    
    func unserialize(json: JSON) -> Post? {
        guard let id = json[Keys.id].int,
            let title = json[Keys.title].string,
            let body = json[Keys.body].string else {
                return nil
        }
        return Post(id: id, title: title, body: body)
    }
    
}
