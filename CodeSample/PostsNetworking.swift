//
//  PostsNetworkin.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

protocol PostsNetworking {
    func getPosts(succesBlock: @escaping PostsSuccess, errorBlock: @escaping PostsFailure)
}
