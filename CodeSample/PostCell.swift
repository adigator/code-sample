//
//  PostCell.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import UIKit

class PostCell: UITableViewCell {
    
    fileprivate let mainView: PostCellView
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        mainView = PostCellView()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.white
        contentView.addSubview(mainView)
        mainView.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPost(post: Post) {
        mainView.title.text = post.title
        mainView.body.text = post.body
    }
    
}

