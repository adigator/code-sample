//
//  BasicTableView.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import UIKit

class BasicTableView: UITableView {
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        rowHeight = UITableViewAutomaticDimension
        estimatedRowHeight = 120
        showsVerticalScrollIndicator = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
