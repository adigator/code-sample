//
//  PostCellView.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

fileprivate struct Constants {
    struct Constraints {
        struct Title {
            static let top = 10
            static let leading = 15
            static let trailing = 15
        }
        struct Body {
            static let top = 10
            static let bottom = 10
        }
    }
}

class PostCellView: UIView {
    
    let title = UILabel()
    let body = UILabel()
    
    func setupView() {
        snp.makeConstraints { (make) in
            make.top.bottom.trailing.leading.equalTo(0)
        }
        setupTitle()
        setupBody()
    }
    
    fileprivate func setupTitle() {
        addSubview(title)
        title.textColor = UIColor.red
        title.numberOfLines = 0
        title.snp.makeConstraints {
            make in
            make.leading.equalTo(0).offset(Constants.Constraints.Title.leading)
            make.trailing.equalTo(0).offset(-Constants.Constraints.Title.trailing)
            make.top.equalTo(0).offset(Constants.Constraints.Title.top)
        }
    }
    
    fileprivate func setupBody() {
        addSubview(body)
        body.numberOfLines = 0
        body.snp.makeConstraints {
            make in
            make.trailing.equalTo(title.snp.trailing)
            make.leading.equalTo(title.snp.leading)
            make.top.equalTo(title.snp.bottom).offset(Constants.Constraints.Body.top)
            make.bottom.equalTo(0).offset(-Constants.Constraints.Body.bottom)
        }
    }
    
}

