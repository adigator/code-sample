//
//  UIViewController+NavigationBar.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 25/09/2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setNavigationBarWithTitle(title: String?) {
        self.title = title
        navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.black
        ]
        navigationController?.navigationBar.barTintColor = UIColor.navigationBar
        navigationController?.navigationBar.backgroundColor = UIColor.navigationBar
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.isOpaque = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIView())
    }
}

