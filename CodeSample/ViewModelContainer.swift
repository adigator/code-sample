//
//  ViewModelContainer.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

class ViewModelContainer {
    
    func getPostsViewModel() -> PostsScreenViewModel {
        let singlePostSerializer = SinglePostSerializer()
        let serializer = PostsSerializer(singlePostSerializer: singlePostSerializer)
        let networking = PostsNetworkingImpl(serializer: serializer)
        let repository = PostsRepositoryImpl(networking: networking)
        return PostsScreenViewModelImpl(repository: repository)
    }
    
}
