//
//  PostsRepository.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

protocol PostsRepository {
    func getPosts(succesBlock: @escaping PostsSuccess, errorBlock: @escaping PostsFailure) -> [Post]?
}
