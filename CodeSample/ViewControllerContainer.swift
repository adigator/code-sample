//
//  ViewControllerContainer.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

class ViewControllerContainer {
    
    fileprivate let viewModelContainer = ViewModelContainer()
    
    func getPostsViewController() -> PostsScreenViewController {
        let viewModel = viewModelContainer.getPostsViewModel()
        return PostsScreenViewController(viewModel: viewModel)
    }
    
}
