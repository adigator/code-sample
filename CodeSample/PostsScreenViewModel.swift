//
//  PostsScreenViewModel.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

protocol PostsScreenViewModel {
    var delegate: CanUpdateContent? { get set }
    
    func getData() -> [Post]     
    func loadData()
}
