//
//  ExampleCache.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

class ExampleCache {
    
    private init() {}
    
    static let cache = NSCache<NSString, AnyObject>()
    
}
