//
//  PostsScreenViewController.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import UIKit

fileprivate struct Constants {
    struct Strings {
        static let navigationBarTitle = "Sample"
    }
}

class PostsScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CanUpdateContent {
    
    fileprivate var viewModel: PostsScreenViewModel
    fileprivate var cellsManager: PostsScreenCellsManager!
    fileprivate let tableView: BasicTableView
    
    init(viewModel: PostsScreenViewModel) {
        self.viewModel = viewModel
        self.tableView = BasicTableView()
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setNavigationBarWithTitle(title: Constants.Strings.navigationBarTitle)
        setupTableView()
        setupCellsManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.loadData()
    }
    
    func updateContent() {
        tableView.reloadData()
    }
    
    fileprivate func setupView() {
        view = tableView
    }
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    fileprivate func setupCellsManager() {
        cellsManager = PostsScreenCellsManager(tableView: tableView)
        assert(cellsManager != nil, "Cells manager not initiated properly")
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getData().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = viewModel.getData()[indexPath.row]
        return cellsManager.buildCell(indexPath: indexPath, post: post)
        
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
