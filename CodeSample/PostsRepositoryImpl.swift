//
//  PostsRepositoryImpl.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

fileprivate struct Constants {
    struct Keys {
        static let posts: NSString = "posts"
    }
}

class PostsRepositoryImpl: PostsRepository {
    
    private let cache = ExampleCache.cache
    private let networking: PostsNetworking
    
    init(networking: PostsNetworking) {
        self.networking = networking
    }
    
    func getPosts(succesBlock: @escaping PostsSuccess, errorBlock: @escaping PostsFailure) -> [Post]? {
        if let cacheObj = cache.object(forKey: Constants.Keys.posts) as? [Post] {
            getContactDataFromApi(succesBlock: succesBlock, errorBlock: errorBlock)
            return cacheObj
        } else {
            getContactDataFromApi(succesBlock: succesBlock, errorBlock: errorBlock)
            return nil
        }
    }
    
    private func getContactDataFromApi(succesBlock: @escaping PostsSuccess, errorBlock: @escaping PostsFailure) {
        networking.getPosts(succesBlock: {
            data in
            self.cache.setObject(data as AnyObject, forKey: Constants.Keys.posts)
            succesBlock(data)
        }, errorBlock: {
            error in
            errorBlock(error?.description)
        })
    }
}
