//
//  PostsNetworkingImpl.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import SwiftyJSON

class PostsNetworkingImpl: BaseNetworking, PostsNetworking {
    
    fileprivate let serializer: PostsSerializer
    
    init(serializer: PostsSerializer) {
        self.serializer = serializer
        super.init()
    }
    
    func getPosts(succesBlock: @escaping PostsSuccess, errorBlock: @escaping PostsFailure) {
        let request = Request(url: makeURL(), method: .get, parameters: nil, headers: nil)
        makeRequest(request: request,
                    succesBlock: getSuccessBlock(successBlock: succesBlock, errorBlock: errorBlock),
                    errorBlock: getErrorBlock(errorBlock: errorBlock))
    }
    
    fileprivate func getSuccessBlock(successBlock: @escaping PostsSuccess, errorBlock: @escaping PostsFailure) -> JSONSuccess {
        return {
            dataJson in
            if let json = dataJson, let items = self.serializer.unserialize(json: json) {
                successBlock(items)
            } else {
                errorBlock(nil)
            }
        }
    }
    
    fileprivate func getErrorBlock(errorBlock: @escaping PostsFailure) -> JSONError {
        return {
            error in
            errorBlock(error.description)
        }
    }
    
    fileprivate func makeURL() -> String {
        return Endpoints.baseURL + Endpoints.posts
    }
    
}


