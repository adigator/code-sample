//
//  Request.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Alamofire

class Request {
    
    var url: URLConvertible
    var method: HTTPMethod
    var parameters: Parameters?
    var headers: HTTPHeaders?
    
    init(url: URLConvertible,
         method: HTTPMethod,
         parameters: Parameters?,
         headers: HTTPHeaders?) {
        
        self.url = url
        self.method = method
        self.parameters = parameters
        self.headers = headers
    }
    
}
