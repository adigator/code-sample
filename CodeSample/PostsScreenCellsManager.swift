//
//  PostsScreenCellsManager.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import UIKit

fileprivate struct Constants {
    struct ReuseIdentifiers {
        static let postCell = "PostCell"
    }
}

class PostsScreenCellsManager {
    
    fileprivate var tableView: UITableView
    
    required init(tableView: UITableView) {
        self.tableView = tableView
        registerCells()
    }
    
    internal func registerCells() {
        tableView.register(PostCell.self, forCellReuseIdentifier: Constants.ReuseIdentifiers.postCell)
    }
    
    func buildCell(indexPath: IndexPath, post: Post) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifiers.postCell, for: indexPath) as? PostCell {
            cell.setPost(post: post)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
}
