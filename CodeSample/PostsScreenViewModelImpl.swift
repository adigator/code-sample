//
//  PostsScreenViewModelImpl.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation

typealias PostsSuccess = (_ data: [Post]) -> ()
typealias PostsFailure = (_ error: String?) -> ()

class PostsScreenViewModelImpl: PostsScreenViewModel {
    
    var delegate: CanUpdateContent?
    var shouldEmptyDataSetDisplay = false
    var data : [Post] = [] {
        didSet {
            delegate?.updateContent()
        }
    }
    
    fileprivate let repository: PostsRepository
    
    required init(repository: PostsRepository) {
        self.repository = repository
    }
    
    func getData() -> [Post] {
        return data
    }
    
    func loadData() {
        if let postsData = repository.getPosts(succesBlock: successBlock(), errorBlock: errorBlock()) {
            self.data = postsData
        }
    }
    
    fileprivate func successBlock() -> PostsSuccess {
        return {
            data in
            self.data = data
        }
    }
    
    fileprivate func errorBlock() -> PostsFailure {
        return {
            (error) in
            self.data = [] // aktualizacja przy np. empy data secie
            print(error)
        }
    }
}
