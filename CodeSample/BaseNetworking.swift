//
//  BaseNetworking.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Alamofire
import SwiftyJSON

typealias JSONSuccess = ((_ json: JSON?) -> ())
typealias JSONError = ((_ error: NSError) -> ())

fileprivate struct Errors {
    static let refreshTokenFetchingErrorTitle = "fetchRefreshTokenErrorTitle"
    static let refreshTokenFetchingErrorMessage = "fetchRefreshTokenErrorMessage"
}

struct Endpoints {
    static let baseURL = "https://jsonplaceholder.typicode.com/"
    static let posts = "posts"
}

class BaseNetworking {
    
    fileprivate let manager: SessionManager
    
    init() {
        let configuraion = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuraion)
    }
    
    func makeRequest(request: Request, succesBlock: @escaping JSONSuccess, errorBlock: @escaping JSONError) {
        manager.request(request.url, method: request.method, parameters: request.parameters, encoding: JSONEncoding.default, headers: request.headers).responseJSON(completionHandler: {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                succesBlock(json)
            case .failure(let error):
                errorBlock(error as NSError)
            }
        })
    }
    
}
