//
//  PostsSerializer.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 09.01.2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import SwiftyJSON

class PostsSerializer {
    
    fileprivate let singlePostSerializer: SinglePostSerializer
    
    init(singlePostSerializer: SinglePostSerializer) {
        self.singlePostSerializer = singlePostSerializer
    }
    
    func unserialize(json: JSON) -> [Post]? {
        if let jsonArray = json.array {
            var posts: [Post] = []
            for jsonElement in jsonArray {
                if let element = singlePostSerializer.unserialize(json: jsonElement) {
                    posts.append(element)
                }
            }
            return posts
        } else {
            return nil
        }
    }
    
}
