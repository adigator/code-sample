//
//  UIColorExtension.swift
//  CodeSample
//
//  Created by Adrian Zdanowicz on 25/09/2017.
//  Copyright © 2017 private. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class var navigationBar: UIColor {
        return UIColor(red: 0.0, green: 114.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
    }
    
}

